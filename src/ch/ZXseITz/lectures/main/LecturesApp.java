package ch.ZXseITz.lectures.main;

import ch.ZXseITz.lectures.main.gui.LecturesUI;
import ch.ZXseITz.lectures.main.models.LecturesPM;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Claudio on 18.06.2017.
 */
public class LecturesApp extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        LecturesPM model = new LecturesPM();
        LecturesController controller = new LecturesController(model);
        LecturesUI ui = new LecturesUI(controller);
        Scene scene = new Scene(ui, 800, 600);

        primaryStage.setScene(scene);
        primaryStage.setTitle(LecturesPM.TITLE);
        primaryStage.show();
    }
}
