package ch.ZXseITz.lectures.main;

import ch.ZXseITz.lectures.main.models.Grade;
import ch.ZXseITz.lectures.main.models.Lecture;
import ch.ZXseITz.lectures.main.models.LecturesPM;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Created by Claudio on 19.06.2017.
 */
public class LecturesController {
    private LecturesPM model;

    private final Lecture proxyLecture;
    private final ObjectProperty<Lecture> currentLecture;

    public LecturesController(LecturesPM model) {
        this.model = model;
        this.proxyLecture = new Lecture();
        this.currentLecture = new SimpleObjectProperty<>();
        this.currentLecture.addListener(((observable, oldValue, newValue) -> rebindProxy(oldValue, newValue)));
    }

    public LecturesPM getModel() {
        return model;
    }

    public Lecture getProxyLecture() {
        return proxyLecture;
    }

    public ObjectProperty<Lecture> currentLectureProperty() {
        return currentLecture;
    }

    /**
     * Rebinds the proxy, unbinds the old lecture, binds to the new lecture
     * @param oldLecture old lecture to unbind
     * @param newLecture new lecture to bind
     */
    private void rebindProxy(Lecture oldLecture, Lecture newLecture) {
        if (oldLecture != null) {
            //unbind
            proxyLecture.abbrevProperty().unbindBidirectional(oldLecture.abbrevProperty());
            proxyLecture.nameProperty().unbindBidirectional(oldLecture.nameProperty());
            proxyLecture.lecturerProperty().unbindBidirectional(oldLecture.lecturerProperty());
            proxyLecture.ectsProperty().unbindBidirectional(oldLecture.ectsProperty());
            proxyLecture.stateProperty().unbindBidirectional(oldLecture.stateProperty());
            proxyLecture.getGrades().clear();
            proxyLecture.mspProperty().unbindBidirectional(oldLecture.mspProperty());
            proxyLecture.mspGradeProperty().unbindBidirectional(oldLecture.mspGradeProperty());
        }
        //bind
        proxyLecture.abbrevProperty().bindBidirectional(newLecture.abbrevProperty());
        proxyLecture.nameProperty().bindBidirectional(newLecture.nameProperty());
        proxyLecture.lecturerProperty().bindBidirectional(newLecture.lecturerProperty());
        proxyLecture.ectsProperty().bindBidirectional(newLecture.ectsProperty());
        proxyLecture.stateProperty().bindBidirectional(newLecture.stateProperty());
        proxyLecture.getGrades().addAll(newLecture.getGrades());
        proxyLecture.mspProperty().bindBidirectional(newLecture.mspProperty());
        proxyLecture.mspGradeProperty().bindBidirectional(newLecture.mspGradeProperty());
    }

    public void addGrade() {
        Grade grade = new Grade();
        proxyLecture.getGrades().add(grade);
        currentLecture.get().getGrades().add(grade);
    }

    public void addLecture() {
        Lecture lecture = new Lecture("Neu", "Neu", "Neu", 3);
        model.getLectures().add(lecture);
        currentLecture.set(lecture);
    }
}