package ch.ZXseITz.lectures.main.models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Claudio on 18.06.2017.
 */
public class LecturesPM {
    public final static String TITLE = "Module";

    private ObservableList<Lecture> lectures;

    public LecturesPM() {
        this.lectures = FXCollections.observableArrayList();
    }

    public ObservableList<Lecture> getLectures() {
        return lectures;
    }
}
