package ch.ZXseITz.lectures.main.models;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Claudio on 18.06.2017.
 */
public class Lecture {
    public enum State {
        NONE,
        PENDING,
        DONE
    }

    private final StringProperty abbrev, name, lecturer;
    private final IntegerProperty ects;
    private final ObjectProperty<State> state;
    private final ObservableList<Grade> grades;
    private final BooleanProperty msp;
    private final DoubleProperty mspGrade;
    private final DoubleProperty finalGrade;

    public Lecture() {
        this.abbrev = new SimpleStringProperty();
        this.name = new SimpleStringProperty();
        this.lecturer = new SimpleStringProperty();
        this.ects = new SimpleIntegerProperty();
        this.state = new SimpleObjectProperty<>(State.NONE);
        this.grades = FXCollections.observableArrayList();
        this.msp = new SimpleBooleanProperty(false);
        this.mspGrade = new SimpleDoubleProperty(0);
        this.finalGrade = new SimpleDoubleProperty(0);
    }

    public Lecture(String abbrev, String name, String lecturer, int etcs) {
        this();
        this.abbrev.set(abbrev);
        this.name.set(name);
        this.lecturer.set(lecturer);
        this.ects.set(etcs);
    }

    public String getAbbrev() {
        return abbrev.get();
    }

    public StringProperty abbrevProperty() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev.set(abbrev);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getLecturer() {
        return lecturer.get();
    }

    public StringProperty lecturerProperty() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer.set(lecturer);
    }

    public int getEcts() {
        return ects.get();
    }

    public IntegerProperty ectsProperty() {
        return ects;
    }

    public void setEcts(int ects) {
        this.ects.set(ects);
    }

    public State getState() {
        return state.get();
    }

    public ObjectProperty<State> stateProperty() {
        return state;
    }

    public void setState(State state) {
        this.state.set(state);
    }

    public ObservableList<Grade> getGrades() {
        return grades;
    }

    public boolean hasMsp() {
        return msp.get();
    }

    public BooleanProperty mspProperty() {
        return msp;
    }

    public void setMsp(boolean msp) {
        this.msp.set(msp);
    }

    public double getMspGrade() {
        return mspGrade.get();
    }

    public DoubleProperty mspGradeProperty() {
        return mspGrade;
    }

    public void setMspGrade(double mspGrade) {
        this.mspGrade.set(mspGrade);
    }

    public double getFinalGrade() {
        return finalGrade.get();
    }

    public ReadOnlyDoubleProperty finalGradeProperty() {
        return finalGrade;
    }
}
