package ch.ZXseITz.lectures.main.models;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Claudio on 18.06.2017.
 */
public class Grade {
    private final StringProperty name;
    private final DoubleProperty quantifier;
    private final DoubleProperty grade;

    public Grade() {
        name = new SimpleStringProperty();
        quantifier = new SimpleDoubleProperty();
        grade = new SimpleDoubleProperty();
    }

    public Grade(String name, double quntifier, double grade) {
        this();
        this.name.set(name);
        this.quantifier.set(quntifier);
        this.grade.set(grade);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getQuantifier() {
        return quantifier.get();
    }

    public DoubleProperty quantifierProperty() {
        return quantifier;
    }

    public void setQuantifier(double quantifier) {
        this.quantifier.set(quantifier);
    }

    public double getGrade() {
        return grade.get();
    }

    public DoubleProperty gradeProperty() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade.set(grade);
    }
}
