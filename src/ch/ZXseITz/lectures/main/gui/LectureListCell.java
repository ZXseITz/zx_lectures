package ch.ZXseITz.lectures.main.gui;

import ch.ZXseITz.lectures.main.models.Lecture;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ListCell;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Created by Claudio on 20.06.2017.
 */
public class LectureListCell extends ListCell<Lecture> {
    private final Circle circle;
    private final ObjectProperty<Lecture.State> state;

    public LectureListCell() {
        circle = new Circle(5);
        state = new SimpleObjectProperty<>();
        state.addListener((observable, oldValue, newValue) -> {
            if (state.get() == Lecture.State.NONE) circle.setFill(Color.BLACK);
            else if (state.get() == Lecture.State.PENDING) circle.setFill(Color.SKYBLUE);
            else {
                //TODO check if passed or failed
                circle.setFill(Color.FORESTGREEN);
            }
        });
    }

    @Override
    protected void updateItem(Lecture item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null && !empty) {
            state.bind(item.stateProperty());
            setGraphic(circle);
            textProperty().bind(item.abbrevProperty().concat("   ").concat(item.getFinalGrade()));
        } else {
            state.unbind();
            setGraphic(null);
            textProperty().unbind();
            setText("");
        }
    }
}
