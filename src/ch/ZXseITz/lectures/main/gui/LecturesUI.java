package ch.ZXseITz.lectures.main.gui;

import ch.ZXseITz.lectures.main.LecturesController;
import ch.ZXseITz.lectures.main.models.Lecture;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

/**
 * Created by Claudio on 18.06.2017.
 */
public class LecturesUI extends BorderPane {
    private final LecturesController controller;
    private final ObjectProperty<Lecture> currentLecture;

    private MenuUI menu;
    private SplitPane content;
    private ListView<Lecture> lectures;
    private LectureUI lecture;


    public LecturesUI(LecturesController controller){
        this.controller = controller;
        this.currentLecture = new SimpleObjectProperty<>();
        initControls();
        layoutControls();
        addListener();
        addBindings();
    }

    private void initControls() {
        menu = new MenuUI(controller);
        content = new SplitPane();
        content.setDividerPosition(0, 0.25);
        lectures = new ListView<>();
        lectures.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lectures.setCellFactory((ListView<Lecture> param) -> new LectureListCell());
        lecture = new LectureUI(controller);
    }

    private void layoutControls() {
        setTop(menu);
        setCenter(content);
        content.getItems().addAll(lectures, lecture);
    }

    private void addListener() {
        currentLecture.addListener((observable, oldValue, newValue) -> lectures.getSelectionModel().select(newValue));
        lectures.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> currentLecture.set(newValue));
    }

    private void addBindings() {
        currentLecture.bindBidirectional(controller.currentLectureProperty());
        lectures.setItems(controller.getModel().getLectures());
    }
}
