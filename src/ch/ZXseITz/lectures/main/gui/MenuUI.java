package ch.ZXseITz.lectures.main.gui;

import ch.ZXseITz.lectures.main.LecturesController;
import javafx.scene.control.*;

/**
 * Created by Claudio on 20.06.2017.
 */
public class MenuUI extends MenuBar {
    private LecturesController controller;

    private Menu file;
    private MenuItem newFile;
    private MenuItem openFile;
    private MenuItem saveFile;

    private Menu edit;
    private MenuItem addLecture;
    private MenuItem addGrade;

    private Menu view;

    private Menu help;

    public MenuUI(LecturesController controller) {
        this.controller = controller;
        initControls();
        layoutControls();
        addListener();
        addBindings();
    }

    private void initControls() {
        file = new Menu("Datei");
        newFile = new MenuItem("neu");
        openFile = new MenuItem("öffnen");
        saveFile = new MenuItem("speichern");

        edit = new Menu("Bearbeiten");
        addLecture = new MenuItem("Modul hinzufügen");
        addGrade = new MenuItem("Note hinzufügen");

        view = new Menu("Ansicht");

        help = new Menu("Hilfe");
    }

    private void layoutControls() {
        file.getItems().addAll(newFile, openFile, saveFile);
        edit.getItems().addAll(addLecture, addGrade);

        getMenus().addAll(file, edit, view, help);
    }

    private void addListener() {
        addLecture.setOnAction(event -> controller.addLecture());
        addGrade.setOnAction(event -> controller.addGrade());
    }

    private void addBindings() {

    }
}
