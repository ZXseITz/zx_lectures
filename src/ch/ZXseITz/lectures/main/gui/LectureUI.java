package ch.ZXseITz.lectures.main.gui;

import ch.ZXseITz.lectures.main.LecturesController;
import ch.ZXseITz.lectures.main.models.Grade;
import ch.ZXseITz.lectures.main.models.Lecture;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.util.converter.NumberStringConverter;

/**
 * Created by Claudio on 19.06.2017.
 */
public class LectureUI extends GridPane {
    private final LecturesController controller;
    private final ObjectProperty<Lecture.State> state;

    private Label lAbbrev, lName, lLecturer, lEcts, lState, lFinal;
    private TextField abbrev, name, lecturer, ects, mspGrade, finalGrade;
    private ComboBox<Lecture.State> states;
    private TableView<Grade> grades;
    private CheckBox msp;

    public LectureUI(LecturesController controller) {
        this.controller = controller;
        this.state = new SimpleObjectProperty<>();
        initControls();
        layoutControls();
        addListener();
        addBindings();
    }

    private void initControls() {
        lAbbrev = new Label("Kürzel");
        lName = new Label("Name");
        lLecturer = new Label("Dozent*in");
        lEcts = new Label("ECTS");
        lState = new Label("Status");
        lFinal = new Label("Schlussnote");

        abbrev = new TextField();
        name = new TextField();
        lecturer = new TextField();
        ects = new TextField();
        states = new ComboBox<>();
        states.getItems().addAll(Lecture.State.NONE, Lecture.State.PENDING, Lecture.State.DONE);
        mspGrade = new TextField();
        finalGrade = new TextField();
        finalGrade.setEditable(false);

        grades = new TableView<>();
        msp = new CheckBox("MSP");
    }

    private void layoutControls() {
        add(lAbbrev, 0, 0);
        add(abbrev, 1, 0);
        add(lName, 0, 1);
        add(name, 1, 1);
        add(lLecturer, 0, 2);
        add(lecturer, 1, 2);
        add(lEcts, 0, 3);
        add(ects, 1, 3);
        add(lState, 0, 4);
        add(states, 1, 4);
        add(grades, 0, 5, 2, 1);
        add(msp, 0, 6);
        add(mspGrade, 1, 6);
        add(lFinal, 0, 7);
        add(finalGrade, 1, 7);

        ColumnConstraints colLabel = new ColumnConstraints();
        colLabel.setPercentWidth(25);
        ColumnConstraints colText = new ColumnConstraints();
        colText.setPercentWidth(75);
        getColumnConstraints().addAll(colLabel, colText);

        RowConstraints rowLabel = new RowConstraints();
        rowLabel.setPercentHeight(10);
        RowConstraints rowGrades = new RowConstraints();
        rowGrades.setPercentHeight(30);
        getRowConstraints().addAll(rowLabel, rowLabel, rowLabel, rowLabel, rowLabel, rowGrades, rowLabel, rowLabel);
    }

    private void addListener() {
        state.addListener((observable, oldValue, newValue) -> states.getSelectionModel().select(newValue));
        states.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> state.set(newValue));
    }

    private void addBindings() {
        Lecture proxy = controller.getProxyLecture();
        abbrev.textProperty().bindBidirectional(proxy.abbrevProperty());
        name.textProperty().bindBidirectional(proxy.nameProperty());
        lecturer.textProperty().bindBidirectional(proxy.lecturerProperty());
        ects.textProperty().bindBidirectional(proxy.ectsProperty(), new NumberStringConverter());
        state.bindBidirectional(proxy.stateProperty());
        grades.setItems(proxy.getGrades());
        msp.selectedProperty().bindBidirectional(proxy.mspProperty());
        mspGrade.textProperty().bindBidirectional(proxy.mspGradeProperty(), new NumberStringConverter());
        finalGrade.textProperty().bind(proxy.finalGradeProperty().asString());
    }
}
